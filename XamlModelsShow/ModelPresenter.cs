﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace XamlModelsShow
{
    public enum ModelType
    {
        RocketFire,

        Cube,
        Soldier,
        Cross,
        Binocular,
        WheelR,
        Scope,
        Bullet,
        TankShell1,
        Rocket,
        BM_8,
        I_16,
        KI_27,
        Soroka,
        Soviet_152,
        GAZ_AA,
        T_37,
        x_152,
        x_155,
        x_37,
        SU_12_1,
        BA6,
        BT5,
        Barrel,
        gun122mm,
        ZIS_3
    }

    /// <summary>
    /// Класс 3Dмодели
    /// </summary>
    public class ModelPresenter : DependencyObject
    {
        public ModelType Type { get; set; }

        #region Angle

        /// <summary>
        /// Угол поворота модели
        /// </summary>
        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Personnel.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(ModelPresenter), new UIPropertyMetadata(null));

        #endregion

        public ModelPresenter(ModelType type, double angle)
        {
            Type = type;
            Angle = angle;
        }

        public ModelPresenter()
        {
        }

    }
}
