﻿using System.Collections.Generic;
using System.Collections;
using AbstractGameEngine;
using GameEngine.Object;
using Geometry.Figures;
using System.Data.SqlClient;
using System.Data.Common;
using System;

namespace GameEngine.Characters.Groups
{
    class DataBaseConnector
    {
        // Строка соединения
        string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDBFilename=Z:\SummerGameBitBucket\GameEngine\Characters\Army.mdf;Integrated Security=true;";
        public UnitFeatures GetUnitFeatures(int armyId)
        {

            var groups = new List<Group>();
            var items = new List<Item>();
            ArrayList newal = new ArrayList();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string query = string.Format("Select Current_id, Count FROM ElementConnection Where Obeys_id='{0}'", armyId);
                SqlCommand com = new SqlCommand(query, con);

                try
                {
                    con.Open();
                }
                catch (Exception e)
                {
                    throw e;
                }

                SqlDataReader dr = com.ExecuteReader();

                newal = getElementIdArmy(con, dr);

                if (dr.HasRows)
                {
                    foreach (DbDataRecord result in dr)
                    {
                        newal.Add(result);
                    }
                }
            }

            groups.Add(new Group(90, Specialization.TankMan));
            groups.Add(new Group(360, Specialization.InfantryMan));
            items.Add(new Goods(ObjectType.Ammunition, 1000, new Circle(0, 0, 0)));

            items.Add(new EquipmentMark(Caliber.smallArms, Caliber.smallArms, 0.35, 3, 1, 12));
            items.Add(new EquipmentMark(Caliber.smallArms, Caliber.smallArms, 0.35, 2, 1, 27));
            items.Add(new EquipmentMark(Caliber.smallArms, Caliber.smallArms, 0.35, 1, 1, 300));
            items.Add(new EquipmentMark(Caliber.smallArms, Caliber.largeSmallArms, 5, 2, 2, 8));
            items.Add(new EquipmentMark(Caliber.small, Caliber.largeSmallArms, 5, 2, 4, 11));
            items.Add(new EquipmentMark(Caliber.medium, Caliber.largeSmallArms, 0.5, 1, 2, 4));
            items.Add(new EquipmentMark(Caliber.smallArms, Caliber.largeSmallArms, 5, 1, 2, 10));
            return new UnitFeatures(groups, items);
        }

        private ArrayList getElementIdArmy(SqlConnection con, SqlDataReader dr)
        {
            ArrayList ids = new ArrayList();


            foreach(DbDataRecord result in dr)
            {
                int elementId = Convert.ToInt32(result.GetValue(1));
                string query = string.Format("Select Current_id, Count FROM ElementConnection Where Obeys_id='{0}'", elementId);
                SqlCommand com = new SqlCommand(query, con);
                SqlDataReader intDr = com.ExecuteReader();

                if (dr.HasRows)
                {
                    getElementIdArmy(con, intDr);
                }
                else
                {

                }

            }

            return ids; 
        }
    }
}
