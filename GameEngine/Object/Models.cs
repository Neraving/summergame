﻿using System.Collections.Generic;
using AbstractGameEngine;
using Geometry;
using Geometry.Figures;

namespace GameEngine.Object
{
    public enum ModelType 
    {
        BA_6,
        GAZ_AA,
        SU_12_1,
        T_37,
        GUN122MM,
        Barrel,
        KI_27,
        GUN152MM,
        GUN152MMSoviet,
        ZIS_3,
        Soroka,
        Warehouse,
        Soldier
    }

    public class Models : Item
    {

        public ModelType Type
        {
            get;
            set;
        }

        public int q;

        public Geometry.Figures.ConvexPolygon polygon
        {
            get;
            set;
        }

        public Models()
        {
        }

        public Models(ModelType Type, Geometry.Figures.ConvexPolygon polygon)
        {
            this.Type = Type;
            this.polygon = polygon;
            q = 0;
        }
    }
}
