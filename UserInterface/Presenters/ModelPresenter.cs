﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using GameEngine.Characters;
using GameEngine.Object;
using GameEngine;
using Geometry;


namespace UserInterface.Presenters
{
    public class ModelPresenter : DependencyObject
    {
        public Models Model
        {
            get;
            set;
        }




        #region IsMyUnit

        public bool IsMyUnit
        {
            get { return (bool)GetValue(IsMyUnitProperty); }
            set { SetValue(IsMyUnitProperty, value); }
        }


        public static readonly DependencyProperty IsMyUnitProperty =
            DependencyProperty.Register("IsMyUnit", typeof(bool), typeof(ModelPresenter), new UIPropertyMetadata(false));

        #endregion


        #region Position

        public FigurePresenter Position
        {
            get { return (FigurePresenter)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Position.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(FigurePresenter), typeof(ModelPresenter), new UIPropertyMetadata(null));

        #endregion

        public ModelPresenter(GameEngine.Object.Models model)
        {
            this.Model = model;

            Position = new FigurePresenter(model.polygon, model);

        }
    }
}
